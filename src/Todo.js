import React from 'react';
import SingleTodo from './SingleTodo'

class Todo extends React.Component{
  constructor(props) {
    super(props);
    localStorage.setItem(this.props.noteText, JSON.stringify([]))
    this.state = {isSaved: 0, todoCount: -1}
  }

  showArray(array1){
    let result = [];
    array1.forEach((item, _index, _array) => {
      result.push(<SingleTodo text={item} deleteTodoHandle={this.deleteTodo}></SingleTodo>);
    });

    return result;
  }

  getFromStorage() {
    const arrText = localStorage.getItem(this.props.noteText);
    return JSON.parse(arrText);
  }  

  renderTodos() {
    const array = this.getFromStorage()
    return this.showArray(array)
  }

  saveToStorage(todo) {
    const arrText = localStorage.getItem(this.props.noteText) || JSON.stringify([]);
    const arr = JSON.parse(arrText);
    arr.push(todo);
    localStorage.setItem(this.props.noteText, JSON.stringify(arr));
    this.setState({isSaved: this.state.isSaved + 1})
  } 

  deleteTodo = (todoToDel) => { 
    const arrText = localStorage.getItem(this.props.noteText);
    const arr = JSON.parse(arrText);
    const search = arr.indexOf(todoToDel)
    arr.splice(search, 1)
    this.setState({todoCount: arr.lenght})
    localStorage.setItem(this.props.noteText, JSON.stringify(arr));
  }

  handleClick = () => {
    const todo = document.getElementById("todo-" + this.props.noteText).value;
    this.saveToStorage(todo);
  }

  render() {
    return(
      <div> 

        {this.renderTodos()}
        <h3>
        <label htmlFor="lname">Тудушка:</label>
        <input type="text" id={"todo-" + this.props.noteText} name="todo" />
        <button className ="button-green" id="btnClick" onClick={this.handleClick}>
          Add Todo
        </button>
        </h3>
      </div>
    )
  }
}
export default Todo;