import './App.css';
import AllNotes from './AllNotes'
import React from 'react';

class App extends React.Component {
  constructor(props) {
    super(props);
    localStorage.setItem("myArray", JSON.stringify([]))
    this.state = {isChanged: true}
  }

  saveToStorage(text) {
    const arrText = localStorage.getItem("myArray") || JSON.stringify([]);
    const arr = JSON.parse(arrText);
    arr.push(text);
    localStorage.setItem("myArray", JSON.stringify(arr));
    this.setState({isChanged: !this.isChanged })
  } 

  handleClick = () => {
    const text = document.getElementById("lname").value;
    this.saveToStorage(text);
  }

  render() {
    return(
      <div>
        <h1>My first App</h1>
        <AllNotes isChanged={this.state.isChanged}></AllNotes>
        <h3>
        <label htmlFor="lname">Заметка:</label>
        <input type="text" id="lname" name="lname" />
        <br/>
        <br/>
        <button className ="button-yellow" id="btnClick" onClick={this.handleClick}>
          Add Note
        </button>
        </h3>
      </div>
    )
  }
}



export default App;
