import React from 'react';

class SingleTodo extends React.Component{
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div>
        <input type="checkbox" id="checktodo" name="todo" />
        <label htmlFor="todo">
          {this.props.text}
        </label>
        <button className ="button-orangered" id="btnClick" onClick={()=>this.props.deleteTodoHandle(this.props.text)}>
          X
        </button>
      </div>
    )
  }
}

export default SingleTodo;
