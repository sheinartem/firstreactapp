import React from 'react';
import Note from './Note'

class AllNotes extends React.Component {
  constructor(props) {
    super(props);
    // this.deleteNote = this.deleteNote.bind(this);
    this.state = {notesCount: -1}
  }

  showArray(array1){
    let result = [];
    array1.forEach((item, index, _array) => {
        result.push(<Note index={index+1} text={item} deleteNoteHandle={this.deleteNote}></Note>);
      });

     return result;
  }

  getFromStorage() {
    const arrText = localStorage.getItem("myArray");
    return JSON.parse(arrText);
  }  

  renderNotes() {
    const array = this.getFromStorage()
    return this.showArray(array)
  }


  deleteNote = (textToDel) => { 
    const arrText = localStorage.getItem("myArray");
    const arr = JSON.parse(arrText);
    const search = arr.indexOf(textToDel)
    arr.splice(search, 1)
    this.setState({notesCount: arr.lenght})
    localStorage.setItem("myArray", JSON.stringify(arr));
  }
 
  render() {
    return (
      <div>
        <h2>AllNotes</h2>
        {this.renderNotes()}
      </div>
    )
  }
}



export default AllNotes;