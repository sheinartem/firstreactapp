import React from 'react';
import Todo from './Todo'

class Note extends React.Component{
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div>  
        <h3>
        {this.props.index}: {this.props.text}
        <button className ="button-red" id="btnClick" onClick={()=>this.props.deleteNoteHandle(this.props.text)}>
          X
        </button>
        <Todo noteText={this.props.text}></Todo>
        </h3>
      </div>

    )
  }
}

export default Note;
